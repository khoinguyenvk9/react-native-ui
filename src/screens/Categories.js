import React from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text
} from 'react-native';
import CategoryListItem from "../components/CategoryListItem";
class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [
                { id: 1, name: "book", icon: "book.png" },
                { id: 2, name: "document", icon: "document.png" },
                { id: 3, name: "paint", icon: "paint.png" },
                { id: 4, name: "pen", icon: "pen.png" },
                { id: 5, name: "tag", icon: "tag.png" },
            ]
        }
    }
    render() {
        const { categories } = this.state;
        return (
            <FlatList
                data={categories}
                renderItem={({ item }) => <CategoryListItem
                    category={item}
                ></CategoryListItem>}
                keyExtractor={item => item.id}
                contentContainerStyle={styles.container}>
            </FlatList>
        );
    }

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: '#fff',
        paddingLeft: 8,
        paddingRight: 8,
    },

});

export default Category;
