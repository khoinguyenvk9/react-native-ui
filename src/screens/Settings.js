import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    FlatList
} from 'react-native';

class Settings extends React.Component {
    render() {

        return (
            <View>
                <Text>Settings</Text>
            </View>
        );
    }

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: '#fff',
        paddingLeft: 8,
        paddingRight: 8,
    },
});

export default Settings;
