import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CategoriesScreen from "./screens/Categories";
import SettingsScreen from "./screens/Settings";
const Tab = createBottomTabNavigator();

export default function AppTabNavigator() {
    return (
        <NavigationContainer>
            <Tab.Navigator>
                <Tab.Screen name="CategoriesScreen" component={CategoriesScreen} />
                <Tab.Screen name="SettingsScreen" component={SettingsScreen} />
            </Tab.Navigator>
        </NavigationContainer>
    );
}