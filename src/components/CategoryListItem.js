import React from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
} from "react-native";
import IconDemo from '../assets/tag.png'
import { useNavigation } from '@react-navigation/native';
export default function CategoryListItem(props) {
    const { category } = props;
    const navigation = useNavigation();
    const name = category.name.toUpperCase();
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => navigation.navigate("Category", { name: name })}>
            <View style={styles.container}>
                <Text style={styles.title}>{category.name}</Text>
                <Image source={IconDemo}>
                </Image>
            </View>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 16,
        shadowColor: '#000',
        shadowOpacity: 0.1,
        shadowRadius: 10,
        shadowOffset: { width: 0, height: 0 },
        borderRadius: 4,
        backgroundColor: '#FFF',
        marginBottom: 16,
    },
    title: {
        textTransform: "uppercase",
        marginBottom: 8,
        fontWeight: '700',
    },

});