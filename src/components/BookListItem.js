import React from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
} from "react-native";
import IconDemo from '../assets/book.png'
import { useNavigation } from '@react-navigation/native';
export default function BookListItem(props) {
    const { book } = props;
    const navigation = useNavigation();
    // onPress={() => navigation.navigate("Category", { name: name })}
    return (
        <TouchableOpacity
            activeOpacity={0.5}
        >
            <View style={styles.container}>
                <Image style={styles.image_card} source={{
                    uri: book.image,
                }}>
                </Image>
                <View style={styles.info}>
                    <View style={{ flexDirection: 'column', width: '100%' }}>
                        <Text style={styles.title}>{book.name}</Text>
                        <Text style={styles.author}>
                            {book.author}
                        </Text>
                    </View>

                    <Text style={styles.desc}>
                        {book.desc}
                    </Text>
                    <View style={styles.price_buy}>
                        <Text style={styles.price}>
                            {book.price}
                        </Text>
                        <TouchableOpacity
                            style={styles.buy}
                            onPress={() => alert("Đã thêm vào giỏ hàng")}>
                            <Text style={styles.btn_buy}>Mua</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({
    container: {

        padding: 5,
        shadowColor: '#000',
        shadowOpacity: 0.1,
        shadowRadius: 10,
        shadowOffset: { width: 0, height: 0 },
        borderRadius: 4,
        backgroundColor: '#FFF',
        marginBottom: 5,
    },
    title: {
        textTransform: "uppercase",
        fontWeight: '900',
    },
    image_card: {
        width: '100%',
        height: 300,
    },
    info: {
        paddingLeft: 10,
        paddingTop: 10,
        paddingRight: 10,
        fontWeight: '700',
    },
    desc: {
        textTransform: 'capitalize',
        fontStyle: "italic",
        paddingTop: 8
    },
    author: {
        fontStyle: 'italic',
        fontWeight: '600'
    },
    price_buy: {
        flexDirection: 'row',
        alignItems: 'center'

    },
    price: {
        flex: 1,
        color: "#FF2222"
    },
    buy: {
        backgroundColor: '#000',
        padding: 8,
        borderRadius: 6,
    },
    btn_buy: {
        color: '#FFF'
    }

});