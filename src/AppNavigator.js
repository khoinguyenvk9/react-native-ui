import React from 'react';
import { Button } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Categories from "./screens/Categories";
import Category from "./screens/Category";
import Settings from "./screens/Settings";

import Icon from 'react-native-vector-icons/FontAwesome'

const StackCategories = createStackNavigator();

function CategoriesStackScreen() {
    return (

        <StackCategories.Navigator>
            <StackCategories.Screen
                name="Categories"
                component={Categories}
                options={{ title: 'Categories' }} />
            <StackCategories.Screen
                name="Category"
                component={Category}
                options={({ route }) => ({
                    title: route.params.name,
                    headerStyle: {
                        backgroundColor: '#FFF',
                    },
                    headerTintColor: '#000',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                    headerRight: () => (
                        <Button
                            onPress={() => alert('This is a button!')}
                            title="+"
                            color="#FFF"

                        />
                    ),
                })} />
        </StackCategories.Navigator>
    );
}
const StackSettings = createStackNavigator();

function SettingsStackScreen() {
    return (

        <StackSettings.Navigator>
            <StackSettings.Screen
                name="Settings"
                component={Settings}
                options={{ title: 'Settings' }} />
        </StackSettings.Navigator>

    );
}

const Tab = createBottomTabNavigator();

export default function AppNavigator() {
    return (
        <NavigationContainer>
            <Tab.Navigator
            // screenOptions={({ route }) => ({
            //     tabBarIcon: ({ focused, color, size }) => {
            //         let iconName;

            //         if (route.name === 'Categories') {
            //             iconName = "comments"
            //         } else if (route.name === 'Settings') {
            //             iconName = "comments";
            //         }
            //         return <Icon name="comments" size={size} color={color} />;
            //     },
            // })}
            // tabBarOptions={{

            // }}

            >
                <Tab.Screen
                    name="Categories"
                    component={CategoriesStackScreen}
                />
                <Tab.Screen
                    name="Settings"
                    component={SettingsStackScreen}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
}

